# Micropub Media Endpoint Proxy

A client to your [Micropub](https://www.w3.org/TR/micropub/) media endpoint to allow uploading of files.

See the announcement blog post at https://www.jvt.me/posts/2020/02/29/media-endpoint-proxy/.

Licensed under the AGPL3.
