'use strict';

const parser = require('lambda-multipart-parser');
const request = require('request');
const { promisify } = require('util')
const post = promisify(request.post);

function validationError(errorMessage) {
	return {
		statusCode: 400,
		body: {
			error_message: errorMessage,
		}
	}
}

exports.handler = async (event) => {
	const result = await parser.parse(event);

	const accessToken = result.access_token;
	if (null === accessToken || 0 === accessToken.length) {
		resolve(validationError('access_token was not defined'));
	}

	if (null === result.files || 0 === result.files.length) {
		resolve(validationError('No file was submitted'));
	}
	const file = result.files[0];
	const mediaEndpoint = result.media_endpoint;
	if (null === mediaEndpoint || 0 === mediaEndpoint.length) {
		resolve(validationError('No media_endpoint was defined'));
	}

	const sendViaBody = result.send_via_body;

	var formData = {
		file: {
			value: file.content,
			options: {
				contentType: file.contentType,
				filename: file.filename,
			}
		}
	};
	var headers = {};

	if (sendViaBody) {
		formData.access_token = accessToken;
	} else {
		headers.authorization = 'Bearer ' + accessToken;
	}

	var options = {
		uri: mediaEndpoint,
		formData: formData,
		headers: headers,
	}

	return new Promise((resolve)=>{
		post(options).then((httpResponse, body) => {
			// as per https://www.w3.org/TR/micropub/#response-3 we only allow a 201 response
			if (201 != httpResponse.statusCode) {
				throw httpResponse;
			}

			// if successful, redirect the browser to the newly uploaded file
			var response = {
				statusCode: 302,
				headers: {
					'location': httpResponse.headers.location
				},
			};
			resolve(response);
		}).catch((err) => {
			console.error("An error occured when proxying the request to `" + err.request.uri.href + "` failed with HTTP status `" + err.statusCode + "` and body `" + err.body + "`");
			// always return the error to the caller
			resolve({
				statusCode: err.statusCode,
				body: err.body,
			});
		});
	});
};
